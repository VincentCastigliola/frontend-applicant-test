app.service('service', function($http) {
    const renderCounts = (data, val) => {
        let totalCounts = 0;
        for (country in data) {
            //Add country's value to total counts. Value can be either Channels or Devices
            totalCounts += data[country][val]
        }
        return totalCounts;
    }
    this.getNames = () => {
        return $http.get('./data.json').then(res => res.data);
    }
    this.getChannelCounts = () => {
        return $http.get('./data.json')
        .then(res => renderCounts(res.data, 'Channels'));
    }
    this.getDeviceCounts = () => {
        return $http.get('./data.json')
        .then(res => renderCounts(res.data, 'Devices'));
    }
});