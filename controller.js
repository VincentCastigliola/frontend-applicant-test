app.controller('myCtrl', function($scope, service){
    service.getNames()
    .then((res) => {
        console.log(res);
        $scope.names = res;
    });

    service.getChannelCounts()
    .then((res) => $scope.totalChannelCounts = res)

    service.getDeviceCounts()
    .then((res) => $scope.totalDeviceCounts = res)

})